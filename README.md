####  Python in Jenkins Pipeline

Create a Jenkins job that fetches all the available images from your application's ECR repository using Python. It allows the user to select the image from the list through user input and deploys the selected image to the EC2 server using Python.

1. Start EC2 instance and install Docker on it
2. Install Python, Pip and all needed Python dependencies in Jenkins
3. Create 3 Docker images with tags 1.0, 2.0, 3.0 from one of the previous projects

Once all the above is configured, create a Jenkins Pipeline with the following steps:

1. Fetch all 3 images from the ECR repository (using Python)
2. Select the image from the list (hint: https://www.jenkins.io/doc/pipeline/steps/pipeline-input-step/)
3. SSH into the EC2 server (using Python)
4. Run docker login to authenticate with ECR repository (using Python)
5. Start the container from the selected image from step 2 on EC2 instance (using Python)
6. Validate that the application was successfully started and is accessible by sending a request to the  application (using Python)


##### Solution
 #Install Python inside Jenkins server
1. apt-get install python3
2. apt-get install pip
3. pip install boto3
4. pip install paramiko
5. pip install requests

 Create credentials in Jenkins 
- "jenkins_aws_access_key_id" - Secret Text
- "jenkins_aws_secret_access_key" - Secret Text
- "ssh-creds" - SSH Username with private key
- "ecr-repo-pwd" - Secret Text

##### Code
 In jenkins folder, you will find the Jenkinsfile that executes 3 python scripts for different stages:
- get-images.py
- deploy.py
- validate.py

 Before executing the Jenkins pipeline, set the following environment variable values inside Jenkinsfile
- ECR_REPO_NAME
- EC2_SERVER
- ECR_REGISTRY
- CONTAINER_PORT
- HOST_PORT
- AWS_DEFAULT_REGION

